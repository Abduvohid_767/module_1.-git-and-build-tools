package com.epam.lab;

/**
 * @author Abduvohid Isroilov
 * @date 2021-06-16
 * @time 18:15
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Result for Utils.isAllPositiveNumbers(\"12\", \"79\") is : " + Utils.isAllPositiveNumbers("12", "79"));
    }

}
