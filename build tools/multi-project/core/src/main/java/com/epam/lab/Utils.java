package com.epam.lab;


import com.epam.lab.custom.utils.StringUtils;

/**
 * @author Abduvohid Isroilov
 * @date 2021-06-16
 * @time 18:00
 */
public class Utils {

    public static boolean isAllPositiveNumbers(String... str) {
        for (String s : str) {
            if (!StringUtils.isPositiveNumber(s))
                return false;
        }
        return true;
    }

}